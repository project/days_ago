<?php

namespace Drupal\days_ago\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Plugin\Field\FieldType\TimestampItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'days_ago_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "days_ago_field_formatter",
 *   label = @Translation("Days ago"),
 *   field_types = {
 *     "datetime",
 *     "timestamp"
 *   }
 * )
 */
class DaysAgoFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $to = new \DateTime();
    $to->setTimestamp(\Drupal::time()->getCurrentTime());
    if ($item->value && $item instanceof TimestampItem) {
      $from = new \DateTime();
      $from->setTimestamp($item->value);
    } else {
      $from = new \DateTime($item->value);
    }
    
    $daysAgo = $to->diff($from)->format("%a");
    
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($daysAgo));
  }

}
